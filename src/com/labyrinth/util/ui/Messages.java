package com.labyrinth.util.ui;


public interface Messages {
    public static final String ACTION_MESSAGE = "Начните осваивать мир. Выберите нужное действие:" +
            "1 - шаг вперёд " +
            "2 - шаг назад \n" +
            "3 - шаг влево " +
            "4 - шаг вправо \n" +
            "5 - вывести карту  " +
            "6 - показать инвентарь\n" +
            "7 - вывести характеристики " +
            "0 - выход из игры \n";

    public static final String GREETINGS_MESSAGE = "Приветствуем Вас в игре Labyrinth Wars, перед вами изображена карта игрового поля";
    public static final String PROFESSION_MESSAGE = "Выберите пожалуйста свою будущую специализацию\n"+
            "1 - Воин\n"+
            "2 - Маг\n"+
            "3 - Разбойник\n"+"0 - Выход\n";
    public static final String INFO_MESSAGE = "";
    public static final String ITEM_CHANGE_QUESTION = "Хотите ли вы заменить текущий артефакт ";
    public static final String ITEM_CHANGE_CHOICE = "1 - заменить, 2 - оставить текущий";
}

