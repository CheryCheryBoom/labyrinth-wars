package com.labyrinth.util.ui;

import java.util.Scanner;


public class ConsoleReader {
    public static final Scanner input = new Scanner(System.in);

    public static int readFromConsole(int max, String message) {
        int choice = 0;
        //do {
            System.out.println(message);
            choice = input.nextInt();
        //} while (choice < max);
        return choice;
    }
}
