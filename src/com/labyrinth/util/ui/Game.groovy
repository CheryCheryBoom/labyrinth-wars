package com.labyrinth.util.ui

import com.labyrinth.domain.IPlayer
import com.labyrinth.domain.Player
import com.labyrinth.domain.World
import com.labyrinth.services.player.PlayerService

/**
 * Created with IntelliJ IDEA.
 * User: Den4ik
 * Date: 24.07.14
 * Time: 21:22
 * To change this template use File | Settings | File Templates.
 */
class Game implements Messages, Runnable {
    World world = new World()
    Player player = new Player()
    PlayerService playerService = new PlayerService(player)

    @Override
    void run() {
        int action
        println(GREETINGS_MESSAGE)
        action = ConsoleReader.readFromConsole(3, PROFESSION_MESSAGE)
        playerService.createProfession(action)
        if (action != 0) {
        world.generate()
        playerService.setStartPosition(world)
        world.show()

        while (action != 0) {
            action = ConsoleReader.readFromConsole(7, ACTION_MESSAGE);
            if (action > 0 && action < 5) player.walk(IPlayer.Direction.values()[action - 1], world);
            else if (action == 5) world.show();
            else if (action == 6) println("${player.getInventory()} \n");
            else if (action == 7) playerService.showCharacteristics();
        }
    } else println("Game over")
}

}
