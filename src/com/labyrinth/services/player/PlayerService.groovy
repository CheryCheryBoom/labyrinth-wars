package com.labyrinth.services.player

import com.labyrinth.domain.IPlayer
import com.labyrinth.domain.Player
import com.labyrinth.domain.World
import com.labyrinth.domain.items.Item


class PlayerService {
    Player player

    PlayerService(Player player) {
        this.player = player
    }


    void setStartPosition(World world) {
        player.position = world.createStartPosition();
        world.clearPosition(player.position)
    }

    void createProfession(int action) {
        switch (action) {
            case 1: player.profession = IPlayer.Profession.WARRIOR
                increaseCharacteristics(15, 120, 40, 7, 5)
                break
            case 2: player.profession = IPlayer.Profession.WIZARD
                increaseCharacteristics(9, 80, 100, 2, 15);
                break
            case 3: player.profession = IPlayer.Profession.ROGUE
                increaseCharacteristics(12, 100, 65, 4, 8);
                break
        }
    }

    void increaseCharacteristics(int damage, int health, int mana, int armor, int MR) {
        player.damage += damage;
        player.health += health;
        player.mana += mana;
        player.armor += armor;
        player.manaRegeneration += MR;
    }

    public void showCharacteristics() {
        print("Profession: " + player.profession);
        //print(" Level: " + level);
        //println(" Exp:" + currentExp + "/" + maxExpForLvl);
        print("Health: " + player.health);
        println(" Damage: " + player.getDamage());
        print("Mana: " + player.mana);
        println(" Mana regeneration: " + player.manaRegeneration);
        println("Armor: " + player.getArmor());
    }
}
