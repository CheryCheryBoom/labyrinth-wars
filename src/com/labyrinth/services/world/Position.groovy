package com.labyrinth.services.world

import com.labyrinth.domain.IPlayer.Direction


class Position implements Messages {
    int x, y

    public Position(int x, int y) {
        this.x = x
        this.y = y
    }

    private boolean maxValue(int value) {
        if (value > 19 || value < 0) {
            println(OUT_OF_MAP_ERROR_MESSAGE)
            true
        } else false

    }

    public void change(Direction direction) {
        switch (direction) {
            case Direction.UP:
                y += 1
                if (maxValue(y)) y -= 1
                break
            case Direction.DOWN:
                y -= 1
                if (maxValue(y))  y += 1
                break
            case Direction.LEFT:
                x -= 1
                if (maxValue(x)) x += 1
                break
            case Direction.RIGHT:
                x += 1
                if (maxValue(x))  x -= 1
                break
        }

    }
}
