package com.labyrinth.domain

import com.labyrinth.domain.IPlayer.Profession
import com.labyrinth.domain.items.Item
import com.labyrinth.domain.items.ItemObjects
import com.labyrinth.services.world.Position

/**
 * Created with IntelliJ IDEA.
 * User: Den4ik
 * Date: 24.07.14
 * Time: 20:15
 * To change this template use File | Settings | File Templates.
 */
class World {
    int[][] map = new int[20][20]
    Random r = new Random();

    void generate() {
        for (i in 0..19)
            for (j in 0..19) map[i][j] = r.nextInt(20)
    }

    void show() {
        for (j in 19..0) {
            println()
            for (i in 0..19) print("${map[i][j]} ")
        }
        println()
    }

    public void clearPosition(Position position) {
        map[position.x][position.y] = 22;
    }

    Position createStartPosition() {
        new Position(r.nextInt(20), r.nextInt(20));
    }

    Item findItem(Position position, Profession profession) {
        int index = map[position.x][position.y]
        if (index < 17 && index > 2){
            switch (profession){
                case Profession.WARRIOR: return ItemObjects.createWarriorItem(index)
                case Profession.WIZARD: return ItemObjects.createWizardItem(index)
                case Profession.ROGUE: return ItemObjects.createRogueItem(index)
                default: null
            }
        } else null
    }
}
