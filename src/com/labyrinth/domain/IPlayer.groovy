package com.labyrinth.domain

import com.labyrinth.domain.IPlayer.Direction
import com.labyrinth.domain.items.Item

/**
 * Created with IntelliJ IDEA.
 * User: Den4ik
 * Date: 24.07.14
 * Time: 14:46
 * To change this template use File | Settings | File Templates.
 */
public interface IPlayer {
    static enum Direction {
        UP, DOWN, LEFT, RIGHT
    }
    static enum Profession {
        WARRIOR, WIZARD, ROGUE
    }
     static enum Slots {
        WEAPON, LEFTHAND, PLATEMAIL, BOOTS
    }
    void walk(Direction direction, World world);

    void attack(boolean isEnemy);

    void grabItem(Item item);

    //void action();


    //void setStartPosition(World world);

}