package com.labyrinth.domain.items


class ItemObjects {


    static Item createWarriorItem(int index) {
        switch (index) {
            case 3: return new Weapon("Wooden Sword", 14, "Effect")
            case 4: return new Weapon("Iron sword", 18, "Iron")
            case 5: return new Weapon("Old guardian sword", 22, "Blade")
            case 7: return new LeftHand("Wooden shield", 3, 5, "Shield effect")
            case 8: return new LeftHand("Iron shield", 4, 7, "Iron")
            case 9: return new LeftHand("Small reflector", 4, 10, "Sphere")
            case 10: return new PlateMail("Apprentice cloth", 0, 4, "Apprentice")
            case 11: return new PlateMail("Old chain mail", 0, 6, "Chain mail")
            case 12: return new PlateMail("Gladiator's breastplate", 0, 9, "Gladiator")
            case 13: return new Boots("Holey shoes", 0, 2, "Holey")
            case 14: return new Boots("Wooden shoes", 0, 3, "Wooden")
            case 15: return new Boots("Iron boots", 0, 4, "Iron")
            default: return null
        }
    }

    static Item createWizardItem(int index) {
        switch (index) {
            case 3: return new Weapon("Magic stick", 11, "Magic");
            case 4: return new Weapon("Wooden staff", 13, "Staff");
            case 5: return new Weapon("Charmed scepter", 16, "Charmed");
            case 7: return new LeftHand("Ancient book", 4, 4, "Ancient");
            case 8: return new LeftHand("Crannied orb", 5, 4, "Crannied");
            case 9: return new LeftHand("Magic stone", 6, 4, "Magic stone");
            case 10: return new PlateMail("Apprentice mantle", 0, 2, "Apprentice mantle");
            case 11: return new PlateMail("Magic robe", 0, 3, "Magic robe");
            case 12: return new PlateMail("Protective cloak", 0, 6, "Gladiator");
            case 13: return new Boots("Holey shoes", 0, 1, "Holey");
            case 14: return new Boots("Fabric shoes", 0, 2, "Fabric");
            case 15: return new Boots("Magic boots", 0, 4, "Magic boots");
            default: return null
        }
    }

    static Item createRogueItem(int index) {
        switch (index) {
            case 3: return new Weapon("Wooden knife", 12, "Effect");
            case 4: return new Weapon("Iron knife", 14, "Iron");
            case 5: return new Weapon("Broken dagger", 17, "Broken");
            case 7: return new LeftHand("Fabric gloves", 0, 2, "Fabric gloves");
            case 8: return new LeftHand("Small hidden blade", 3, 0, "SHB");
            case 9: return new LeftHand("Spiked gloves", 3, 3, "Spiked");
            case 10: return new PlateMail("Unlucky robber cloth", 0, 3, "Unlucky");
            case 11: return new PlateMail("Leather mail", 0, 5, "Leather skin");
            case 12: return new PlateMail("Robe from best skin", 0, 7, "Best");
            case 13: return new Boots("Holey shoes", 0, 1, "Holey");
            case 14: return new Boots("Wooden sandals", 0, 3, "Wooden sandals");
            case 15: return new Boots("Boots of speed", 0, 3, "Boots of speed");
            default: return null
        }
    }

    static Item createDefaultItem(int index) {
        switch (index) {
            case 0: return new Weapon("Fist", 0, "Fist effect")
            case 1: return new LeftHand("Fist", 0, 0, "Effect")
            case 2: return new PlateMail("Peasant shirt", 0, 0, "Peasant")
            case 3: return new Boots("Peasant sandals", 0, 0, "Sandals")
            default: return null
        }
    }



}