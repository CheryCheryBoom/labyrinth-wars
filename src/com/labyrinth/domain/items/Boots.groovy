package com.labyrinth.domain.items

/**
 * Created with IntelliJ IDEA.
 * User: Den4ik
 * Date: 25.07.14
 * Time: 14:47
 * To change this template use File | Settings | File Templates.
 */
class Boots extends Item {
    public Boots(String name, int bonusEffect, int armor, String effect) {
        this.itemName = name;
        this.armor = armor;
        this.bonusEffect=bonusEffect;
        this.effect=effect;
    }

    int bonusEffect, armor;
    String effect, itemName;
}
