package com.labyrinth.domain.items

/**
 * Created with IntelliJ IDEA.
 * User: Den4ik
 * Date: 25.07.14
 * Time: 11:45
 * To change this template use File | Settings | File Templates.
 */
class Item {
    String itemName;
    int damage = 0;
    int health = 0;
    int mana = 0;
    int armor = 0;
    int manaRegeneration = 0;
}
