package com.labyrinth.domain

import com.labyrinth.domain.items.Boots
import com.labyrinth.domain.items.Item
import com.labyrinth.domain.items.ItemObjects
import com.labyrinth.domain.items.LeftHand
import com.labyrinth.domain.items.PlateMail
import com.labyrinth.domain.items.Weapon
import com.labyrinth.services.world.Position
import com.labyrinth.util.ui.InventoryChecker
import static com.labyrinth.domain.IPlayer.Slots.*


class Player implements IPlayer {
    Position position
    Map<IPlayer.Slots, Item> inventory = [(WEAPON):ItemObjects.createDefaultItem(0), (LEFTHAND): ItemObjects.createDefaultItem(1), (PLATEMAIL): ItemObjects.createDefaultItem(2), (BOOTS): ItemObjects.createDefaultItem(2)]
    //Characteristics
    int damage, health, mana, armor, manaRegeneration;
    int level = 0, maxExpForLvl = 100, currentExp;
    IPlayer.Profession profession


    @Override
    void walk(IPlayer.Direction direction, World world) {
        if (position){
            position.change(direction)
            grabItem(world.findItem(position,profession))
            world.clearPosition(position)

        }

    }

    @Override
    void attack(boolean isEnemy) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    void grabItem(Item item) {
        if (item == Weapon)InventoryChecker.changeItemQuestion(inventory, WEAPON, item)
        if (item == LeftHand) InventoryChecker.changeItemQuestion(inventory, LEFTHAND, item)
        if (item == PlateMail) InventoryChecker.changeItemQuestion(inventory, PLATEMAIL, item)
        if (item == Boots) InventoryChecker.changeItemQuestion(inventory, BOOTS, item)

    }



    Collection<Item> getInventory() {
        inventory.values();
    }

    int getDamage() {
        println(damage + "1")
        println(inventory.get(WEAPON).damage + "2")
        println(inventory.get(LEFTHAND).damage + "3")
        return damage + inventory.get(WEAPON).damage + inventory.get(LEFTHAND).damage
    }

    int getArmor() {
        return armor + inventory.get(PLATEMAIL).armor + inventory.get(LEFTHAND).armor + inventory.get(BOOTS).armor
    }
}
